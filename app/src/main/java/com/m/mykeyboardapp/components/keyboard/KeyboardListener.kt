package com.m.mykeyboardapp.components.keyboard

import com.m.mykeyboardapp.components.keyboard.controllers.KeyboardController

interface KeyboardListener {
    fun characterClicked(c: Char)
    fun specialKeyClicked(key: KeyboardController.SpecialKey)
}