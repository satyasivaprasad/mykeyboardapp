package com.m.mykeyboardapp.components.expandableView

enum class ExpandableState {
    COLLAPSED,
    COLLAPSING,
    EXPANDED,
    EXPANDING
}