package com.m.mykeyboardapp.components.expandableView

interface ExpandableStateListener {
    fun onStateChange(state: ExpandableState)
}